package cz.upce.arduinointerface.controller;


import cz.upce.arduinointerface.serialport.ArduinoPortManager;
import cz.upce.arduinointerface.stage.StageManager;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class ArduinoInterfaceController implements Initializable {
    final static Logger logger = LogManager.getLogger(ArduinoInterfaceController.class);
    @Autowired
    private ArduinoPortManager arduinoPortManager;
    @Autowired
    private StageManager stageManager;

    @FXML
    private TextArea windowTextArea;

    @FXML
    private ComboBox<String> portsComboBox;

    @FXML
    private Button listenButton;

    @FXML
    private Button stopButton;

    @FXML
    private Button sendButton;

    @FXML
    void handleSendButton(ActionEvent event) {

    }

    @FXML
    void handleListenButtonAction(ActionEvent event) {
        arduinoPortManager.openAndListen(portsComboBox.getSelectionModel().getSelectedIndex());
        listenButton.setDisable(true);
        windowTextArea.setText("");
        windowTextArea.setEditable(false);
        stopButton.setDisable(false);
    }

    @FXML
    void handleStopButtonAction(ActionEvent event) {
        arduinoPortManager.closePort();
        listenButton.setDisable(false);
        windowTextArea.setEditable(true);
        stopButton.setDisable(true);
    }

    public void setSendTextAreaText(String textAreaText){
        windowTextArea.appendText(textAreaText);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        portsComboBox.setItems(FXCollections.observableList(arduinoPortManager.getPortNames()));
        portsComboBox.getSelectionModel().selectFirst();
        stopButton.setDisable(true);
    }


}
