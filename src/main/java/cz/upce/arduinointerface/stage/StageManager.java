package cz.upce.arduinointerface.stage;

import cz.upce.arduinointerface.application.ArduinoInterfaceApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class StageManager {
    @Autowired
    private ConfigurableApplicationContext configurableApplicationContext;

    private Parent primaryRoot;


    private Stage primaryStage;

    final static Logger logger = LogManager.getLogger(ArduinoInterfaceApp.class);



    public void showPrimaryStage(){
        try {
            primaryRoot = load("/ArduinoInterfaceView.fxml");
        }catch (IOException e){
            logger.error(e.fillInStackTrace(),e);
        }

        primaryStage.setTitle("Arduino Interface");
        Scene scene = new Scene(primaryRoot, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setResizable(false);
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }



    private Parent load(String fxmlFilePath) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setControllerFactory(configurableApplicationContext::getBean);
        fxmlLoader.setLocation(getClass().getResource(fxmlFilePath));
        return fxmlLoader.load();
    }

}
