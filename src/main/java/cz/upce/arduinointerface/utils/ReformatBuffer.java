package cz.upce.arduinointerface.utils;


import cz.upce.arduinointerface.controller.ArduinoInterfaceController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReformatBuffer {
    @Autowired
    private ArduinoInterfaceController arduinoInterfaceController;
    private static int outOfASCII = 10;
    private static String temporaryBuffer = "";

    public void parseByteArray(byte[] readBuffer) {
        String readBufferSniplet = new String(readBuffer);
        temporaryBuffer = temporaryBuffer.concat(readBufferSniplet);

        if ((temporaryBuffer.indexOf(outOfASCII) + 1) > 0) {
            String outputString = temporaryBuffer.substring(0, temporaryBuffer.indexOf(outOfASCII) + 1);
            temporaryBuffer = temporaryBuffer.substring(temporaryBuffer.indexOf(outOfASCII) + 1);
            arduinoInterfaceController.setSendTextAreaText(outputString);
        }
    }


}
