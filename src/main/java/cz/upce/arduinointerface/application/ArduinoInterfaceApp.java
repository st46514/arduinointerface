package cz.upce.arduinointerface.application;


import cz.upce.arduinointerface.stage.StageManager;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cz.upce.arduinointerface")
@SpringBootApplication
public class ArduinoInterfaceApp extends Application {

    private ConfigurableApplicationContext configurableApplicationContext;

    final static Logger logger = LogManager.getLogger(ArduinoInterfaceApp.class);


    private StageManager stageManager;

    @Override
    public void init() {
        configurableApplicationContext = SpringApplication.run(ArduinoInterfaceApp.class);
        stageManager = configurableApplicationContext.getBean(StageManager.class);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        configurableApplicationContext.stop();
        logger.info("Stopping application");

    }

    @Override
    public void start(Stage primaryStage) {
        stageManager.setPrimaryStage(primaryStage);
        stageManager.showPrimaryStage();
        logger.info("Starting application");

    }

    public static void main(String[] args) {
        launch(ArduinoInterfaceApp.class,args);
    }
 @Bean
    public ConfigurableApplicationContext getConfigurableApplicationContext() {
        return configurableApplicationContext;
    }


}
