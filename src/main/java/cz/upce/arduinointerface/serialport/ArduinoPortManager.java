package cz.upce.arduinointerface.serialport;

import com.fazecast.jSerialComm.SerialPort;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class ArduinoPortManager {
    private SerialPort arduinoPort = null;
    private InputStream arduinoPortInput = null;
    private OutputStream arduinoPortOutput = null;
    final static Logger logger = LogManager.getLogger(ArduinoPortManager.class);
    @Autowired
    private ArduinoListener arduinoListener;

    public List<String> getPortNames(){
        int numberOfPorts = SerialPort.getCommPorts().length;
        SerialPort serialPort[] = SerialPort.getCommPorts();
        List<String> portNames = new ArrayList();
        for (int i=0;i<numberOfPorts;i++){
            portNames.add(serialPort[i].getDescriptivePortName());
        }
        return portNames;
    }

    private void openPort(int i) {
        arduinoPort = SerialPort.getCommPorts()[i];
        arduinoPort.openPort();
        arduinoPortOutput = arduinoPort.getOutputStream();
        arduinoPortInput = arduinoPort.getInputStream();
        try{
        arduinoPortInput.skip(arduinoPortInput.available());
        } catch (IOException e){
            logger.error(e.fillInStackTrace(),e);
        }
        logger.info("Connected to " + arduinoPort.getDescriptivePortName());
    }

    public void openAndListen(int i){
        openPort(i);
        arduinoPort.addDataListener(arduinoListener);
    }

    public void openAndWrite(int i,String text){
        byte[] bytes = text.getBytes();
        openPort(i);
        if (arduinoPortOutput!=null) {
            try {
                arduinoPortOutput.write(bytes);
                logger.info("Wrote " + text + " to " + arduinoPort.getDescriptivePortName());
            }catch (IOException e){
                logger.error(e.fillInStackTrace(),e);
            }
        }
    }

    public void closePort(){
        if (arduinoPort!=null) {
            arduinoPort.closePort();
            logger.info("Closing port " + arduinoPort.getDescriptivePortName());
        }
    }
}
